﻿﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
namespace wf2
{
    public partial class Form1 : Form
    {
        string lokacija;
        Vjesala Igra;
        public Form1()
        {
            InitializeComponent();
            lokacija = "ii.txt";
            Igra = new Vjesala(15, lokacija);

            broj_preostalih.Text = Igra.preostalo.ToString();

            rijec.Text = Igra.ispis_x;

        }

        private void label1_Click(object sender, EventArgs e)
        {//riječ

        }

        private void button1_Click(object sender, EventArgs e)
        {//nova
            textBox_slovo.Text = "";
            textBox_rijec.Text = "";

            Igra = new Vjesala(15, lokacija);
            broj_preostalih.Text = Igra.preostalo.ToString();
            rijec.Text = Igra.ispis_x;
        }

        private void textBox_slovo_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_rijec_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {//

        }

        private void label2_Click(object sender, EventArgs e)
        {//

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Igra.pokusaj_slovo(textBox_slovo.Text);
            broj_preostalih.Text = Igra.preostalo.ToString();
            rijec.Text = Igra.ispis_x;
        }

        private void broj_preostalih_Click(object sender, EventArgs e)
        {
        }

        private void try_word_Click(object sender, EventArgs e)
        {
            Igra.pokusaj_rijec(textBox_rijec.Text);
            broj_preostalih.Text = Igra.preostalo.ToString();
            rijec.Text = Igra.ispis_x;
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }
    }



    public class Rjecnik
    {
        private string loc;
        private int n;
        public Rjecnik(string lokacija)
        {
            loc = lokacija;
            int temp = 0;
            var fileStream = new FileStream(loc, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    temp++;
                }
            }
            this.n = temp;

        }
        public int getN() { return n; }
    }

    public class Vjesala : Rjecnik
    {
        bool[] abeceda = new bool[31];
        public string rijec;
        public int preostalo;
        public string ispis;
        public string ispis_x;

        public Vjesala(int br_pokusaja, string lokacija) : base(lokacija)
        {
            preostalo = br_pokusaja;
            Random random = new Random();
            int rand = random.Next(0, getN()-1);
            int temp = 0;
            for (int i = 0; i < 30; i++) abeceda[i] = false;
            var fileStream = new FileStream(lokacija, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (temp == rand) rijec = line;
                    temp++;
                }
                rijec = rijec.ToUpper();
            }
            ispis = "";
            for (int i = 0; i < rijec.Length; i++)
            {
                if (rijec[i] == ' ')
                {
                    ispis += " ";
                    ispis_x += "  ";
                }
                else
                {
                    ispis += "_";
                    ispis_x += "_ ";
                }
            }
        }


        public void pokusaj_slovo(string pokusaj)
        {
            if (ispis != rijec)
            {
                if (preostalo > 0)
                {
                    if (pokusaj.Length != 1) MessageBox.Show("Treba unijeti jedno slovo.", "Greška");
                    else
                    {
                        char slovo = pokusaj[0];
                        if ((slovo >= 65 && slovo <= 90) || (slovo >= 97 && slovo <= 122) || slovo == 'Š' || slovo == 'Đ' || slovo == 'Č' || slovo == 'Ć' || slovo == 'Ž' || slovo == 'č' || slovo == 'ć' || slovo == 'ž' || slovo == 'š' || slovo == 'đ')
                        {
                            if (slovo >= 97 && slovo <= 122)
                            {
                                int temp;
                                temp = (int)slovo - 32;
                                slovo = (char)temp;
                            }

                            if (slovo == 'š') slovo = 'Š';
                            if (slovo == 'đ') slovo = 'Đ';
                            if (slovo == 'č') slovo = 'Č';
                            if (slovo == 'ć') slovo = 'Ć';
                            if (slovo == 'ž') slovo = 'Ž';

                            int slovo_int = 0;
                            if (slovo == 'Š' || slovo == 'š') slovo_int = 26;
                            else if (slovo == 'Đ') slovo_int = 27;
                            else if (slovo == 'Č') slovo_int = 28;
                            else if (slovo == 'Ć') slovo_int = 29;
                            else if (slovo == 'Ž') slovo_int = 30;
                            else if (slovo >= 65 && slovo <= 90) slovo_int = (int)slovo - 65;
                            if (abeceda[slovo_int] == true)
                            {
                                MessageBox.Show("Već je probano to slovo.", "Duplanje");
                            }
                            else
                            {
                                //ovdje ide pogađanje slova
                                for (int i = 0; i < rijec.Length; i++)
                                {
                                    //rijec.Text = ispis_x;
                                    if (rijec[i] == slovo)
                                    {
                                        //MessageBox.Show("Ima tog slova.");
                                        char[] temp = ispis.ToCharArray();
                                        temp[i] = rijec[i];
                                        ispis = new string(temp);
                                        //rijec.Text = ispis;

                                        ispis_x = "";
                                        ispis_x += ispis;
                                        ispis_x += ispis;
                                        //ispis_x += ispis;

                                        char[] ispiss = ispis_x.ToCharArray();

                                        for (int j = 0; j < ispis.Length; j++)
                                        {
                                            ispiss[2 * j] = ispis[j];
                                            ispiss[2 * j + 1] = ' ';
                                            //ispiss[3 * j + 2] = ' ';
                                        }
                                        ispis_x = new string(ispiss);
                                        //rijec.Text = ispis_x;
                                        if (ispis == rijec)
                                        {
                                            MessageBox.Show("Uspjeh.");
                                        }
                                    }
                                }
                                abeceda[slovo_int] = true;
                                preostalo--;
                                if (preostalo == 0) MessageBox.Show("Nemate više raspoloživih pokušaja.", "Kraj igre");
                            }
                        }
                        else MessageBox.Show("Nije unijeto slovo engleskog jezika.", "Greška");
                    }
                }
                else MessageBox.Show("Nemate više raspoloživih pokušaja.", "Kraj igre");
            }
            else MessageBox.Show("Igra je več gotova.");
        }

        public void pokusaj_rijec(string pokusaj)
        {
            if (preostalo > 0)
            {
                if (pokusaj != "")
                {
                    if (pokusaj.Length == rijec.Length)
                    {
                        //textBox_rijec.Text = textBox_rijec.Text.ToUpper();
                        if (pokusaj.ToUpper() == rijec)
                        {
                            ispis = rijec;
                            ispis_x = "";
                            ispis_x += ispis;
                            ispis_x += ispis;
                            //ispis_x += ispis;

                            char[] ispiss = ispis_x.ToCharArray();

                            for (int j = 0; j < ispis.Length; j++)
                            {
                                ispiss[2 * j] = ispis[j];
                                ispiss[2 * j + 1] = ' ';
                                //ispiss[2 * j + 2] = ' ';
                            }
                            ispis_x = new string(ispiss);

                            //ispis_x;
                            MessageBox.Show("Točan odgovor.", "Kraj igre");

                        }
                        else
                        {
                            preostalo--;
                            if (preostalo == 0) MessageBox.Show("Nemate više raspoloživih pokušaja.", "Kraj igre");
                        }
                    }
                    else MessageBox.Show("Niste unijeli riječ iste dužine kao i zadana riječ.", "Greška");
                }
                else MessageBox.Show("Polje je prazno", "Greška");
            }
            else MessageBox.Show("Nemate više raspoloživih pokušaja.", "Kraj igre");
        }
    }
}
